package com.newsapp.businessLayer.converter;

import org.hamcrest.core.Is;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.List;

import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.helper.Constants;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConverterImplTest {

    @Test
    public void testConverterImpl_ListFromJSONArray() throws Exception {

        JSONArray jsonArrayMock =  mock(JSONArray.class);
        doReturn(1)
                .when(jsonArrayMock).length();
        JSONObject newsEntityJson = prepareNewsEntityJSONObject();
        doReturn(newsEntityJson)
                .when(jsonArrayMock).get(0);

        doReturn(newsEntityJson)
                .when(jsonArrayMock).getJSONObject(0);

        IConverter IConverter = new ConverterImpl();
        List<NewsEntity> idList = IConverter.convertToNewsList(jsonArrayMock);

        assertThat(idList.size(), Is.is(equalTo(1)));
    }

    private JSONObject prepareNewsEntityJSONObject() throws Exception {
        JSONObject jsonObjectMock = mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_DESC)).thenReturn(true);
        String mockDesc = "Due to launch next month in Singapore, CarouPay will allow buyers and sellers to complete transactions without leaving the Carousell app, rather than having to rely on third-party platforms or doing meet-ups to hand over cash. CarouPay will be a digital wallet within the Carousell app. \\\"More than half of our sellers will end up buying items as well, so maybe it makes sense to have that money in the wallet for purchases\\\" - Quek tells Tech in Asia.";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_DESC)).thenReturn(mockDesc);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        String mockID = "121";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        when(jsonObjectMock.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        int mockRank = 2;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_RANK)).thenReturn(mockRank);


        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        long mockTime = 1532853058;
        when(jsonObjectMock.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(false);
        String mockTitle = "Carousell is launching its own digital wallet to improve payments for its users";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(false);
        String mockURL = "https://storage.googleapis.com/carousell-interview-assets/android/images/carousell-siu-rui-ceo-tia-sg-2018.jpg";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(), is(equalTo("")));
        assertThat(newsEntity.getDescription(), is(equalTo(mockDesc)));
        assertThat(newsEntity.getBannerUrl(), is(equalTo("")));
        assertThat(newsEntity.getTimeCreated(), is(equalTo(mockTime)));
        assertThat(newsEntity.getId(), is(equalTo(mockID)));
        assertThat(newsEntity.getRank(), is(equalTo(mockRank)));
        return jsonObjectMock;
    }

}