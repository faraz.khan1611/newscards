package com.newsapp.businessLayer.entity;

import org.json.JSONObject;
import org.junit.Test;

import com.newsapp.helper.Constants;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NewsEntityTest {

    private final String mockTitle = "Carousell is launching its own digital wallet to improve payments for its users";
    private final String mockID = "121";
    private final int mockRank = 2;
    private final String mockDesc = "Due to launch next month in Singapore, CarouPay will allow buyers and sellers to complete transactions without leaving the Carousell app, rather than having to rely on third-party platforms or doing meet-ups to hand over cash. CarouPay will be a digital wallet within the Carousell app. \\\"More than half of our sellers will end up buying items as well, so maybe it makes sense to have that money in the wallet for purchases\\\" - Quek tells Tech in Asia.";
    private final long mockTime = 1532853058;
    private final String mockURL = "https://storage.googleapis.com/carousell-interview-assets/android/images/carousell-siu-rui-ceo-tia-sg-2018.jpg";


    @Test
    public void testNewsEntity_AllKeyAndValue() throws Exception {
        JSONObject jsonObjectMock = mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_DESC)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_DESC)).thenReturn(mockDesc);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        when(jsonObjectMock.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_RANK)).thenReturn(mockRank);


        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        when(jsonObjectMock.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(), is(equalTo(mockTitle)));
        assertThat(newsEntity.getDescription(), is(equalTo(mockDesc)));
        assertThat(newsEntity.getBannerUrl(), is(equalTo(mockURL)));
        assertThat(newsEntity.getTimeCreated(), is(equalTo(mockTime)));
        assertThat(newsEntity.getId(), is(equalTo(mockID)));
        assertThat(newsEntity.getRank(), is(equalTo(mockRank)));
    }

    @Test
    public void testNewsEntity_MissingKeyAndValue_For_Title_URL() throws Exception {
        JSONObject jsonObjectMock = mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_DESC)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_DESC)).thenReturn(mockDesc);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        when(jsonObjectMock.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_RANK)).thenReturn(mockRank);


        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        when(jsonObjectMock.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(false);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(false);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(), is(equalTo("")));
        assertThat(newsEntity.getDescription(), is(equalTo(mockDesc)));
        assertThat(newsEntity.getBannerUrl(), is(equalTo("")));
        assertThat(newsEntity.getTimeCreated(), is(equalTo(mockTime)));
        assertThat(newsEntity.getId(), is(equalTo(mockID)));
        assertThat(newsEntity.getRank(), is(equalTo(mockRank)));
    }

}
