package com.newsapp.businessLayer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.newsapp.businessLayer.data.DataRepositoryImpl;
import com.newsapp.businessLayer.data.IDataRepository;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.INetwork;
import com.newsapp.helper.Constants;
import com.newsapp.businessLayer.converter.ConverterImpl;
import com.newsapp.businessLayer.converter.IConverter;
import com.newsapp.businessLayer.network.Error;
import com.newsapp.businessLayer.network.INetworkCallbackInterface;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class InteractorTest {

    private final String mockID = "121";

    @Captor
    private ArgumentCaptor<INetworkCallbackInterface<JSONArray>> callbackJSONArrayCaptor;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    public void loadNewsList_Success() throws Exception {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        IInteractorInterface interactorLayer = new Interactor(dataRepository);
        doNothing().when(networkLayer).fetchJSONArrayWithURL(anyString(), callbackJSONArrayCaptor.capture());

        JSONArray jsonArrayMock =  mock(JSONArray.class);
        doReturn(1)
                .when(jsonArrayMock).length();
        JSONObject newsEntityJson = prepareNewsEntityJSONObject();
        doReturn(newsEntityJson)
                .when(jsonArrayMock).get(0);
        doReturn(newsEntityJson)
                .when(jsonArrayMock).getJSONObject(0);


        interactorLayer.loadNewsList(new InteractorConsumer() {

            @Override
            public void accept(List<NewsEntity> data) {
                assertThat(data.get(0).getId(),is(equalTo(mockID)));
            }

            @Override
            public void reject(Error error) {

            }
        });

        doCallRealMethod().when(converter).convertToNewsList(jsonArrayMock);
        doCallRealMethod().when(converter).convertToNewsEntity(any(JSONObject.class));
        callbackJSONArrayCaptor.getValue().onSuccess(jsonArrayMock);//calling network success to verify the happy path
        verify(converter,atLeastOnce()).convertToNewsList(jsonArrayMock);
    }

    @Test
    public void loadNewsList_RecentSort() throws Exception {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        IInteractorInterface interactorLayer = new Interactor(dataRepository);
        JSONObject jsonObjectMock_1 = mock(JSONObject.class);
        when(jsonObjectMock_1.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        long mockTime = 1560017718L;
        when(jsonObjectMock_1.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);
        NewsEntity newsEntity_1 = new NewsEntity(jsonObjectMock_1);

        JSONObject jsonObjectMock_2 = mock(JSONObject.class);
        when(jsonObjectMock_2.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        mockTime = 1559412964;
        when(jsonObjectMock_2.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);
        NewsEntity newsEntity_2 = new NewsEntity(jsonObjectMock_2);

        JSONObject jsonObjectMock_3 = mock(JSONObject.class);
        when(jsonObjectMock_3.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        mockTime = 1554142564;
        when(jsonObjectMock_3.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);
        final NewsEntity newsEntity_3 = new NewsEntity(jsonObjectMock_3);

        JSONObject jsonObjectMock_4 = mock(JSONObject.class);
        when(jsonObjectMock_4.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        mockTime = 1560104167;
        when(jsonObjectMock_4.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);
        final NewsEntity newsEntity_4 = new NewsEntity(jsonObjectMock_4);
        List<NewsEntity> mockList = Arrays.asList(newsEntity_1,newsEntity_2,newsEntity_3,newsEntity_4);
        interactorLayer.loadNewsListRecentlySort(mockList,new InteractorConsumer() {

            @Override
            public void accept(List<NewsEntity> data) {
                assertThat(data.get(0).getTimeCreated(),is(equalTo(newsEntity_4.getTimeCreated())));
                assertThat(data.get(3).getTimeCreated(),is(equalTo(newsEntity_3.getTimeCreated())));
            }

            @Override
            public void reject(Error error) {

            }
        });

    }

    @Test
    public void loadNewsList_PopularSort() throws Exception {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        IInteractorInterface interactorLayer = new Interactor(dataRepository);
        JSONObject jsonObjectMock_1 = mock(JSONObject.class);
        when(jsonObjectMock_1.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        long mockTime = 1560017718L;

        when(jsonObjectMock_1.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        int mockRank = 1;
        when(jsonObjectMock_1.getInt(Constants.JSON_NEWS_RANK)).thenReturn(mockRank);

        when(jsonObjectMock_1.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);
        final NewsEntity newsEntity_1 = new NewsEntity(jsonObjectMock_1);

        JSONObject jsonObjectMock_2 = mock(JSONObject.class);
        when(jsonObjectMock_2.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        mockTime = 1559412964;

        when(jsonObjectMock_2.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        mockRank = 2;
        when(jsonObjectMock_2.getInt(Constants.JSON_NEWS_RANK)).thenReturn(mockRank);
        when(jsonObjectMock_2.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);
        final NewsEntity newsEntity_2 = new NewsEntity(jsonObjectMock_2);

        JSONObject jsonObjectMock_3 = mock(JSONObject.class);
        when(jsonObjectMock_3.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        mockTime = 1554142564;

        when(jsonObjectMock_3.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        mockRank = 2;
        when(jsonObjectMock_3.getInt(Constants.JSON_NEWS_RANK)).thenReturn(mockRank);
        when(jsonObjectMock_3.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);
        final NewsEntity newsEntity_3 = new NewsEntity(jsonObjectMock_3);

        JSONObject jsonObjectMock_4 = mock(JSONObject.class);
        when(jsonObjectMock_4.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        mockTime = 1560104167;

        when(jsonObjectMock_4.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        mockRank = 3;
        when(jsonObjectMock_4.getInt(Constants.JSON_NEWS_RANK)).thenReturn(mockRank);
        when(jsonObjectMock_4.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);
        final NewsEntity newsEntity_4 = new NewsEntity(jsonObjectMock_4);
        List<NewsEntity> mockList = Arrays.asList(newsEntity_1,newsEntity_2,newsEntity_3,newsEntity_4);
        interactorLayer.loadNewsListPopularSort(mockList,new InteractorConsumer() {

            @Override
            public void accept(List<NewsEntity> data) {
                assertThat(data.get(0).getTimeCreated(),is(equalTo(newsEntity_1.getTimeCreated())));
                assertThat(data.get(1).getTimeCreated(),is(equalTo(newsEntity_2.getTimeCreated())));
                assertThat(data.get(2).getTimeCreated(),is(equalTo(newsEntity_3.getTimeCreated())));
            }

            @Override
            public void reject(Error error) {

            }
        });

    }

    @Test
    public void loadNewsList_Failure() {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        IInteractorInterface interactorLayer = new Interactor(dataRepository);
        doNothing().when(networkLayer).fetchJSONArrayWithURL(anyString(), callbackJSONArrayCaptor.capture());
        interactorLayer.loadNewsList(new InteractorConsumer() {

            @Override
            public void accept(List<NewsEntity> data) {

            }

            @Override
            public void reject(Error error) {
                assertThat(error.getMessage(),is(equalTo("Test Error")));
            }
        });
        callbackJSONArrayCaptor.getValue().onError(new Error("Test Error"));//calling network success to verify the happy path
        verify(converter,times(0)).convertToNewsList(any(JSONArray.class));
    }

    private JSONObject prepareNewsEntityJSONObject() throws Exception {
        JSONObject jsonObjectMock = mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_DESC)).thenReturn(true);
        String mockDesc = "Due to launch next month in Singapore, CarouPay will allow buyers and sellers to complete transactions without leaving the Carousell app, rather than having to rely on third-party platforms or doing meet-ups to hand over cash. CarouPay will be a digital wallet within the Carousell app. \\\"More than half of our sellers will end up buying items as well, so maybe it makes sense to have that money in the wallet for purchases\\\" - Quek tells Tech in Asia.";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_DESC)).thenReturn(mockDesc);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        when(jsonObjectMock.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        int mockRank = 2;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_RANK)).thenReturn(mockRank);


        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        long mockTime = 1532853058;
        when(jsonObjectMock.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(false);
        String mockTitle = "Carousell is launching its own digital wallet to improve payments for its users";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(false);
        String mockURL = "https://storage.googleapis.com/carousell-interview-assets/android/images/carousell-siu-rui-ceo-tia-sg-2018.jpg";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(), is(equalTo("")));
        assertThat(newsEntity.getDescription(), is(equalTo(mockDesc)));
        assertThat(newsEntity.getBannerUrl(), is(equalTo("")));
        assertThat(newsEntity.getTimeCreated(), is(equalTo(mockTime)));
        assertThat(newsEntity.getId(), is(equalTo(mockID)));
        assertThat(newsEntity.getRank(), is(equalTo(mockRank)));
        return jsonObjectMock;
    }

    private JSONObject prepareNewsEntityJSONObject(int rank, long time) throws Exception {
        JSONObject jsonObjectMock = mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_DESC)).thenReturn(true);
        String mockDesc = "Due to launch next month in Singapore, CarouPay will allow buyers and sellers to complete transactions without leaving the Carousell app, rather than having to rely on third-party platforms or doing meet-ups to hand over cash. CarouPay will be a digital wallet within the Carousell app. \\\"More than half of our sellers will end up buying items as well, so maybe it makes sense to have that money in the wallet for purchases\\\" - Quek tells Tech in Asia.";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_DESC)).thenReturn(mockDesc);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        when(jsonObjectMock.getString(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        when(jsonObjectMock.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        int mockRank = 2;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_RANK)).thenReturn(rank);


        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        long mockTime = 1532853058;
        when(jsonObjectMock.getLong(Constants.JSON_NEWS_TIME)).thenReturn(time);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(false);
        String mockTitle = "Carousell is launching its own digital wallet to improve payments for its users";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(false);
        String mockURL = "https://storage.googleapis.com/carousell-interview-assets/android/images/carousell-siu-rui-ceo-tia-sg-2018.jpg";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(), is(equalTo("")));
        assertThat(newsEntity.getDescription(), is(equalTo(mockDesc)));
        assertThat(newsEntity.getBannerUrl(), is(equalTo("")));
        assertThat(newsEntity.getTimeCreated(), is(equalTo(mockTime)));
        assertThat(newsEntity.getId(), is(equalTo(mockID)));
        assertThat(newsEntity.getRank(), is(equalTo(mockRank)));
        return jsonObjectMock;
    }


}
