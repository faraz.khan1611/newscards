package com.newsapp.businessLayer.utils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class UtilsTest {

    private final long mockNowTimeMillis = 1559837349000L;
    @Test
    public void testGetTimeToDisplayStringForMillis_YearTest()
    {
        long timeInMillis =  1528300709L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,mockNowTimeMillis);
        assertThat(timeToDisplay,is(equalTo("1 year")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_YearsTest()
    {
        long timeInMillis =  1496765054L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,mockNowTimeMillis);
        assertThat(timeToDisplay,is(equalTo("2 years")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_MonthTest()
    {
        long timeInMillis =  1557158669L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,mockNowTimeMillis);
        assertThat(timeToDisplay,is(equalTo("1 month")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_MonthsTest()
    {
        long timeInMillis =  1554566669L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,mockNowTimeMillis);
        assertThat(timeToDisplay,is(equalTo("2 months")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_DayTest()
    {
        long timeInMillis =  1559750736L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,mockNowTimeMillis);
        assertThat(timeToDisplay,is(equalTo("1 day")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_DaysTest()
    {
        long timeInMillis =  1559577998L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,mockNowTimeMillis);
        assertThat(timeToDisplay,is(equalTo("3 days")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_WeekTest()
    {
        long timeInMillis =  1559497659L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,1560102562L * 1000L);
        assertThat(timeToDisplay,is(equalTo("1 week")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_4WeekTest()
    {
        long timeInMillis =  1557424523L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,1560102562L * 1000L);
        assertThat(timeToDisplay,is(equalTo("1 month")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_HourTest() {
        long timeInMillis = 1559833674L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,mockNowTimeMillis);
        assertThat(timeToDisplay,is(equalTo("1 hour")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_HoursTest()
    {
        long timeInMillis =  1559826504L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,mockNowTimeMillis);
        assertThat(timeToDisplay,is(equalTo("3 hours")));
    }

    @Test
    public void testGetTimeToDisplayStringForMillis_MinsTest()
    {
        long timeInMillis =  1559837109L * 1000L;
        String timeToDisplay = Utils.getRelativeTimeSince(timeInMillis,mockNowTimeMillis);
        assertThat(timeToDisplay,is(equalTo("4 mins")));
    }
}
