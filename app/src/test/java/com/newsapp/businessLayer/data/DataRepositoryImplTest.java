package com.newsapp.businessLayer.data;

import com.newsapp.businessLayer.converter.ConverterImpl;
import com.newsapp.businessLayer.converter.IConverter;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;
import com.newsapp.businessLayer.network.INetwork;
import com.newsapp.businessLayer.network.INetworkCallbackInterface;
import com.newsapp.helper.Constants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DataRepositoryImplTest {

    @Captor
    private ArgumentCaptor<INetworkCallbackInterface<JSONArray>> callbackJSONArrayCaptor;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    public void Test_getNewsList_Success() throws Exception {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        doNothing().when(networkLayer).fetchJSONArrayWithURL(anyString(), callbackJSONArrayCaptor.capture());
        dataRepository.getNewsList(new IDataRepository.IRepoListener<List<NewsEntity>>() {
            @Override
            public void accept(List<NewsEntity> data) {
                assertThat(data.size(),is(equalTo(1)));
            }

            @Override
            public void reject(Error error) {

            }
        });
        JSONArray jsonArrayMock =  mock(JSONArray.class);
        doReturn(1)
                .when(jsonArrayMock).length();
        JSONObject newsEntityJson = prepareNewsEntityJSONObject();
        doReturn(newsEntityJson)
                .when(jsonArrayMock).get(0);
        doReturn(newsEntityJson)
                .when(jsonArrayMock).getJSONObject(0);


        doCallRealMethod().when(converter).convertToNewsList(any(JSONArray.class));
        doCallRealMethod().when(converter).convertToNewsEntity(any(JSONObject.class));
        callbackJSONArrayCaptor.getValue().onSuccess(jsonArrayMock);//calling network success to verify the happy path
        verify(converter,atLeastOnce()).convertToNewsList(jsonArrayMock);
    }

    @Test
    public void Test_getNewsList_Error() {
        INetwork networkLayer = mock(INetwork.class);
        IConverter converter = mock(ConverterImpl.class);
        IDataRepository dataRepository = new DataRepositoryImpl(networkLayer,converter);
        doNothing().when(networkLayer).fetchJSONArrayWithURL(anyString(), callbackJSONArrayCaptor.capture());
        dataRepository.getNewsList(new IDataRepository.IRepoListener<List<NewsEntity>>() {
            @Override
            public void accept(List<NewsEntity> data) {
            }

            @Override
            public void reject(Error error) {
                assertThat(error.getMessage(),is(equalTo("Something went wrong")));

            }
        });
        doCallRealMethod().when(converter).convertToNewsList(any(JSONArray.class));
        callbackJSONArrayCaptor.getValue().onError(new Error("Something went wrong"));//calling network success to verify the happy path
        verify(converter,times(0)).convertToNewsList(any(JSONArray.class));
    }

    private JSONObject prepareNewsEntityJSONObject() throws Exception {
        JSONObject jsonObjectMock = mock(JSONObject.class);
        when(jsonObjectMock.has(Constants.JSON_NEWS_DESC)).thenReturn(true);
        String mockDesc = "Due to launch next month in Singapore, CarouPay will allow buyers and sellers to complete transactions without leaving the Carousell app, rather than having to rely on third-party platforms or doing meet-ups to hand over cash. CarouPay will be a digital wallet within the Carousell app. \\\"More than half of our sellers will end up buying items as well, so maybe it makes sense to have that money in the wallet for purchases\\\" - Quek tells Tech in Asia.";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_DESC)).thenReturn(mockDesc);

        when(jsonObjectMock.has(Constants.JSON_NEWS_ID)).thenReturn(true);
        String mockID = "121";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_ID)).thenReturn(mockID);

        when(jsonObjectMock.has(Constants.JSON_NEWS_RANK)).thenReturn(true);
        int mockRank = 2;
        when(jsonObjectMock.getInt(Constants.JSON_NEWS_RANK)).thenReturn(mockRank);


        when(jsonObjectMock.has(Constants.JSON_NEWS_TIME)).thenReturn(true);
        long mockTime = 1532853058;
        when(jsonObjectMock.getLong(Constants.JSON_NEWS_TIME)).thenReturn(mockTime);

        when(jsonObjectMock.has(Constants.JSON_NEWS_TITLE)).thenReturn(false);
        String mockTitle = "Carousell is launching its own digital wallet to improve payments for its users";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_TITLE)).thenReturn(mockTitle);

        when(jsonObjectMock.has(Constants.JSON_NEWS_URL)).thenReturn(false);
        String mockURL = "https://storage.googleapis.com/carousell-interview-assets/android/images/carousell-siu-rui-ceo-tia-sg-2018.jpg";
        when(jsonObjectMock.getString(Constants.JSON_NEWS_URL)).thenReturn(mockURL);

        NewsEntity newsEntity = new NewsEntity(jsonObjectMock);

        assertThat(newsEntity.getTitle(), is(equalTo("")));
        assertThat(newsEntity.getDescription(), is(equalTo(mockDesc)));
        assertThat(newsEntity.getBannerUrl(), is(equalTo("")));
        assertThat(newsEntity.getTimeCreated(), is(equalTo(mockTime)));
        assertThat(newsEntity.getId(), is(equalTo(mockID)));
        assertThat(newsEntity.getRank(), is(equalTo(mockRank)));
        return jsonObjectMock;
    }

}
