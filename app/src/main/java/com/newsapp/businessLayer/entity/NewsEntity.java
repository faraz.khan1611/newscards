package com.newsapp.businessLayer.entity;

import androidx.annotation.NonNull;

import com.newsapp.businessLayer.utils.JSONUtils;
import com.newsapp.businessLayer.utils.Utils;
import com.newsapp.helper.Constants;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

public class NewsEntity {
	private final String description;
	private final int rank;
	private final String bannerUrl;
	private final String id;

	private final long timeCreated;
	private final String title;
	private final String timeToDisplay;

	public NewsEntity(@NonNull JSONObject jsonObject) {
		description = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_NEWS_DESC);
		id = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_NEWS_ID);
		timeCreated = JSONUtils.getLongFromJSON(jsonObject, Constants.JSON_NEWS_TIME, 0);
		title = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_NEWS_TITLE);
		bannerUrl = JSONUtils.getStringFromJSON(jsonObject, Constants.JSON_NEWS_URL);
		rank = JSONUtils.getIntFromJSON(jsonObject,Constants.JSON_NEWS_RANK,0);
		long millis = (timeCreated * 1000);
		timeToDisplay = Utils.getRelativeTimeSince(millis,System.currentTimeMillis());
	}

	public String getDescription(){
		return description;
	}

	public int getRank(){
		return rank;
	}

	public String getBannerUrl(){
		return bannerUrl;
	}

	public String getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}

	public long getTimeCreated() {
		return timeCreated;
	}

	@NotNull
	@Override
 	public String toString(){
		return 
			"NewsEntity{" + 
			"description = '" + description + '\'' +
			",rank = '" + rank + '\'' + 
			",banner_url = '" + bannerUrl + '\'' + 
			",id = '" + id + '\'' + 
			",title = '" + title + '\'' + 
			"}";
		}

	public String getTimeToDisplay() {
		return timeToDisplay;
	}
}
