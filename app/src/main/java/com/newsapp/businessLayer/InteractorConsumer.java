package com.newsapp.businessLayer;

import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;

import java.util.List;

public interface InteractorConsumer {
    void accept(List<NewsEntity> data);
    void reject(Error error);
}
