package com.newsapp.businessLayer.network;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
/**
 * INetwork implementation for network access
 * Created by Faraz Khan.
 */

public class NetworkImpl implements INetwork {

    @Override
    public void fetchJSONArrayWithURL(final String URL, final INetworkCallbackInterface<JSONArray> networkCallback) {
        Runnable task = new Runnable() {

            final WeakReference<INetworkCallbackInterface<JSONArray>> weakNetworkCallback = new WeakReference<>(networkCallback);

            @Override
            public void run() {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    String readStream = readStream(con.getInputStream());
                    JSONArray jsonArray;
                    jsonArray = new JSONArray(readStream);
                    if (weakNetworkCallback.get() != null) {
                        weakNetworkCallback.get().onSuccess(jsonArray);
                    }
                } catch (Exception e) {
                    if (weakNetworkCallback.get() != null) {
                        weakNetworkCallback.get().onError(new Error(e.getMessage()));
                    }
                }
            }
        };
        AsyncOperation.postAsync(task);
    }

    private String readStream(InputStream in) {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {

            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
