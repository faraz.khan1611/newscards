package com.newsapp.businessLayer.network;
/**
 * INetworkCallbackInterface interface for network access
 * Created by Faraz Khan.
 */
public interface INetworkCallbackInterface<T> {
    //Use T in order to convert Network Response to required Type like JSON,XML etc.
    void onSuccess(T result);

    void onError(Error error);
}
