package com.newsapp.businessLayer.network;

import org.json.JSONArray;
import org.json.JSONObject;

public interface INetwork {

    void fetchJSONArrayWithURL(String URL, INetworkCallbackInterface<JSONArray> networkCallback);
}