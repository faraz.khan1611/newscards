package com.newsapp.businessLayer;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.newsapp.businessLayer.data.IDataRepository;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;

/**
 * IInteractorInterface implementation for layer above data repo for UI specific data manipulation
 * Created by Faraz Khan.
 */

public class Interactor implements IInteractorInterface {

    private final IDataRepository mRepository;

    private final Comparator<NewsEntity> recentSort = new Comparator<NewsEntity>(){
        @Override
        public int compare(NewsEntity o1, NewsEntity o2) {
            return Long.compare(o2.getTimeCreated(), o1.getTimeCreated());
        }
    };

    private final Comparator<NewsEntity> popularSort = new Comparator<NewsEntity>(){

        @Override
        public int compare(NewsEntity o1, NewsEntity o2) {
            if(o1.getRank() > o2.getRank())
            {
                return 1;
            }
            else if(o1.getRank() < o2.getRank())
            {
                return -1;
            }
            else
            {
                return Long.compare(o2.getTimeCreated(), o1.getTimeCreated());
            }
        }

    };

    public Interactor(IDataRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public void loadNewsListRecentlySort(List<NewsEntity> list, InteractorConsumer consumer) {
        Collections.sort(list,recentSort);
        consumer.accept(list);
    }

    @Override
    public void loadNewsListPopularSort(List<NewsEntity> list, InteractorConsumer consumer) {
        Collections.sort(list,popularSort);
        consumer.accept(list);

    }

    @Override
    public void loadNewsList(final InteractorConsumer consumer) {
        mRepository.getNewsList(new IDataRepository.IRepoListener<List<NewsEntity>>() {

            final WeakReference<InteractorConsumer> listener = new WeakReference<>(consumer);

            @Override
            public void accept(List<NewsEntity> list) {
                InteractorConsumer cb = listener.get();
                if (cb != null) {
                    cb.accept(list);
                }
            }

            @Override
            public void reject(Error error) {
                InteractorConsumer cb = listener.get();
                if (cb != null) {
                    cb.reject(error);
                }
            }
        });
    }

    @Override
    public NewsEntity getDetailedNewsForID(String id) {
        return mRepository.getNewsEntityForId(id);
    }


}
