package com.newsapp.businessLayer.data;

import com.newsapp.businessLayer.entity.NewsEntity;

import java.util.List;

/**
 * The Cache implementaion.
 * Stores obj for faster access
 * Created by Faraz Khan.
 */

interface ICache {
    void setCacheData(List<NewsEntity> list);
    NewsEntity getCachedDataFor(String id);
}
