package com.newsapp.businessLayer.data;

import android.annotation.SuppressLint;

import com.newsapp.businessLayer.entity.NewsEntity;

import java.util.HashMap;
import java.util.List;

/**
 * The Cache implementaion.
 * Stores obj for faster access
 * Created by Faraz Khan.
 */

class Cache implements ICache{

    @SuppressLint("UseSparseArrays")
    final private HashMap<String, NewsEntity> newsMap = new HashMap<>();

    @Override
    public void setCacheData(List<NewsEntity> list) {
        for(NewsEntity entity:list)
        {
            newsMap.put(entity.getId(),entity);
        }
    }

    @Override
    public NewsEntity getCachedDataFor(String id) {
        return newsMap.get(id);
    }
}
