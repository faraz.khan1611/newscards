package com.newsapp.businessLayer.data;

import com.newsapp.businessLayer.converter.IConverter;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;
import com.newsapp.businessLayer.network.INetworkCallbackInterface;
import com.newsapp.businessLayer.network.INetwork;
import com.newsapp.helper.Constants;

import org.json.JSONArray;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * The Data Repository implementaion.
 * Allows abstraction for Cache and Network
 * Created by Faraz Khan.
 */

public class DataRepositoryImpl implements IDataRepository {

    private final INetwork mNetwork;
    private final IConverter mConverter;
    private final ICache mCache = new Cache();

    public DataRepositoryImpl(INetwork network, IConverter converter) {
        this.mNetwork = network;
        this.mConverter = converter;

    }

    @Override
    public void getNewsList(final IRepoListener<List<NewsEntity>> callback) {
        this.mNetwork.fetchJSONArrayWithURL(Constants.NEWS_URL, new INetworkCallbackInterface<JSONArray>() {

            final WeakReference<IConverter> weakConverter = new WeakReference<>(mConverter);
            final WeakReference<IRepoListener<List<NewsEntity>>> listener = new WeakReference<>(callback);

            @Override
            public void onSuccess(JSONArray result) {
                IConverter converter = weakConverter.get();
                if (converter != null) {
                    List<NewsEntity> newsList = converter.convertToNewsList(result);
                    mCache.setCacheData(newsList);
                    IRepoListener<List<NewsEntity>> cb = listener.get();
                    if (cb != null) {
                        cb.accept(newsList);
                    }
                }
            }

            @Override
            public void onError(Error error) {
                IRepoListener cb = listener.get();
                if (cb != null) {
                    cb.reject(error);
                }
            }
        });
    }

    @Override
    public NewsEntity getNewsEntityForId(String id) {
        return mCache.getCachedDataFor(id);
    }
}
