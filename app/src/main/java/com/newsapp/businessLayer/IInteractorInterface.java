package com.newsapp.businessLayer;

import com.newsapp.businessLayer.entity.NewsEntity;

import java.util.List;

/**
 * IInteractorInterface for layer above data repo for UI specific data manipulation
 * Created by Faraz Khan.
 */
public interface IInteractorInterface {
    void loadNewsList(InteractorConsumer consumer);
    void loadNewsListRecentlySort(List<NewsEntity> list, InteractorConsumer consumer);
    void loadNewsListPopularSort(List<NewsEntity> list, InteractorConsumer consumer);
    NewsEntity getDetailedNewsForID(String id);
}
