package com.newsapp.businessLayer.utils;
/**
 * Created by Faraz Khan.
 */
public class Utils {

    private static final long SECOND = 1000;
    private static final long MINUTE = 60 * SECOND;
    private static final long HOUR = 60 * MINUTE;
    private static final long DAY = 24 * HOUR;
    private static final long WEEK = 7 * DAY;
    private static final long MONTH = 30L * DAY;
    private static final long YEAR = 12L * MONTH;

    //Below methods returns the relative String using time
    // eg 2 hours ago etc
    public static String getRelativeTimeSince(long since, long now) {
        long millis = now - since;
        StringBuilder text = new StringBuilder();


        long years =  (millis / YEAR);
        if(years > 0)
        {
            if (years == 1) {
                text.append("1 year");
            } else {
                text.append(years).append(" years");
            }
            return text.toString();
        }

        long months = (millis / MONTH) % 30;
        if(months > 0)
        {
            if (months == 1) {
                text.append("1 month");
            } else {
                text.append(months).append(" months");
            }
            return text.toString();
        }

        long weeks = (millis / WEEK) % 7;
        if(weeks > 0) {
            if(weeks ==1)
            {
                text.append(weeks).append(" week");
                return text.toString();
            }
            else if (weeks < 4) {
                text.append(weeks).append(" weeks");
                return text.toString();
            }
        }

        long days = (millis / DAY) % 24;
        if (days > 0) {
            if (days == 1) {
                text.append("1 day");
            }else {
                text.append(days).append(" days");
            }
            return text.toString();
        }

        long hours = (millis / HOUR) % 60;
        if(hours > 0)
        {
            if (hours == 1) {
                text.append("1 hour");
            } else {
                text.append(hours).append(" hours");
            }
            return text.toString();
        }


        long minutes = (millis / MINUTE) % 60;
        if(minutes > 0)
        {
            text.append(minutes).append(" mins");
            return text.toString();
        }

        long seconds = (millis / 1000) % 60;
        if(seconds > 0)
        {
            text.append(seconds).append(" seconds");
            return text.toString();
        }

        if(millis == 0)
        {
            text.append("0 mins");
        }
        return text.toString();
    }
}
