package com.newsapp.businessLayer.converter;

import com.newsapp.businessLayer.entity.NewsEntity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * The Converter class.
 * Allows object translation
 * Created by Faraz Khan.
 */


public class ConverterImpl implements IConverter {

    @Override
    public NewsEntity convertToNewsEntity(@NotNull JSONObject result){
        NewsEntity entity;
        entity = new NewsEntity(result);
        return entity;
    }

    @Override
    public List<NewsEntity> convertToNewsList(@NotNull JSONArray result) {
        List<NewsEntity> list = new ArrayList<>();
        try {
            for (int i = 0; i < result.length(); i++) {
                NewsEntity entity = this.convertToNewsEntity(result.getJSONObject(i));
                list.add(entity);
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return list;
    }
}
