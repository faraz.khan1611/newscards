package com.newsapp.businessLayer.converter;

import com.newsapp.businessLayer.entity.NewsEntity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * The Converter interface.
 * Allows object translation
 * Created by Faraz Khan.
 */

public interface IConverter {
    NewsEntity convertToNewsEntity(@NotNull JSONObject result);
    List<NewsEntity> convertToNewsList(@NotNull JSONArray result);
}
