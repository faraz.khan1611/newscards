package com.newsapp.helper;

/**
 * Created by Faraz Khan.
 */

public class Constants {
    //region app
    public final static String NEWS_URL = "https://storage.googleapis.com/carousell-interview-assets/android/carousell_news.json";
    //endregion

    //region JSON Keys
    public final static String JSON_NEWS_ID= "id";
    public final static String JSON_NEWS_TITLE = "title";
    public final static String JSON_NEWS_DESC = "description";
    public final static String JSON_NEWS_URL = "banner_url";
    public final static String JSON_NEWS_TIME = "time_created";
    public final static String JSON_NEWS_RANK= "rank";
    //endregion
}
