package com.newsapp.view.activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.news.sample.R;
import com.newsapp.businessLayer.entity.NewsEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Faraz Khan.
 */

public class NewsListRecyclerViewAdapter extends RecyclerView.Adapter<NewsListRecyclerViewAdapter.ViewHolder> {


    private final List<NewsEntity> dataModelList = new ArrayList<>();

    public void loadNewsList(List<NewsEntity> list)
    {
        dataModelList.clear();
        dataModelList.addAll(list);
        notifyDataSetChanged();
    }

    public List<NewsEntity> getNewsList()
    {
        return dataModelList;
    }

    @NonNull
    @Override
    public NewsListRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsListRecyclerViewAdapter.ViewHolder holder, int position) {
        NewsEntity entity = dataModelList.get(position);
        holder.bindNewsView(entity);
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView title;
        final TextView desc;
        final TextView timeToDisplay;
        final SimpleDraweeView banner;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.news_title);
            desc = itemView.findViewById(R.id.news_desc);
            timeToDisplay = itemView.findViewById(R.id.news_time);
            banner = itemView.findViewById(R.id.news_banner);
        }

        void bindNewsView(final NewsEntity entity)
        {
            this.title.setText(entity.getTitle());
            this.desc.setText(entity.getDescription());
            this.timeToDisplay.setText(String.format("%s ago", entity.getTimeToDisplay()));
            this.banner.setImageURI(entity.getBannerUrl());

        }
    }
}
