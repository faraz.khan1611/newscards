package com.newsapp.view.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.news.sample.R;
import com.newsapp.businessLayer.IInteractorInterface;
import com.newsapp.businessLayer.InteractorConsumer;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;
import com.newsapp.dependencies.DependencyRegistry;
import com.newsapp.view.viewModels.NewsDataViewModel;
import com.newsapp.view.viewModels.NewsEntityModel;
import com.newsapp.view.viewModels.NewsEntityState;

import java.util.List;

/**
 * Created by Faraz Khan.
 */

public class MainActivity extends AppCompatActivity {

    private IInteractorInterface mInteractor;
    private NewsListRecyclerViewAdapter adapter;
    private RecyclerView newsRecycleView;
    private NewsDataViewModel mViewModel;
    private ProgressBar progressBar;
    private TextView loaderText;
    private Toast toast;
    private SwipeRefreshLayout pullToRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        newsRecycleView = findViewById(R.id.recyclerViewNews);
        progressBar = findViewById(R.id.progressBar);
        loaderText = findViewById(R.id.loading);
        mViewModel = ViewModelProviders.of(this).get(NewsDataViewModel.class);
        DependencyRegistry.getInstance().inject(this);
    }

    //region Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_sort_popular) {
            mInteractor.loadNewsListPopularSort(adapter.getNewsList(), new InteractorConsumer() {
                @Override
                public void accept(List<NewsEntity> data) {
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void reject(Error error) {

                }
            });
        }else if(id == R.id.action_sort_recent)
        {
            mInteractor.loadNewsListRecentlySort(adapter.getNewsList(), new InteractorConsumer() {
                @Override
                public void accept(List<NewsEntity> data) {
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void reject(Error error) {

                }
            });
        }

        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region Initialize
    public void configureInteractor(IInteractorInterface mInteractor) {
        this.mInteractor = mInteractor;
        loadInitialView();
        initObservers();
    }

    private void initObservers() {
        mViewModel.getNewsListLiveData().observe(this, new Observer<NewsEntityModel>() {
            @Override
            public void onChanged(NewsEntityModel listNewsEntityModel) {
                if (listNewsEntityModel.getState() == NewsEntityState.SUCCESS) {
                    adapter.loadNewsList(listNewsEntityModel.getEntity());
                } else {
                    //Handle error scenarios
                    if (toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(getBaseContext(), "Error found :: " + listNewsEntityModel.getError().getMessage(), Toast.LENGTH_SHORT);
                    toast.show();
                }
                hideProgress();
            }
        });
    }

    @SuppressLint("DefaultLocale")
    private void hideProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            loaderText.setVisibility(View.GONE);
        }
    }

    private void loadInitialView() {
        adapter = new NewsListRecyclerViewAdapter();
        newsRecycleView.setAdapter(adapter);
        if (mViewModel != null) {
            mViewModel.getNewsList(mInteractor);
        }
        pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mViewModel != null) {
                    mViewModel.getNewsList(mInteractor);
                }
                pullToRefresh.setRefreshing(false);
            }
        });
    }
    //endregion
}
