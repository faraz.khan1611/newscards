package com.newsapp.view.viewModels;
/**
 * Created by Faraz Khan.
 */

public enum NewsEntityState {
    SUCCESS,
    FAIL
}
