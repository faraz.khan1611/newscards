package com.newsapp.view.viewModels;

import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;

import java.util.List;

/**
 * Created by Faraz Khan.
 */

public class NewsEntityModel {
    private final NewsEntityState state;
    private final List<NewsEntity> entity;
    private final Error error;

    public NewsEntityState getState() {
        return state;
    }

    public List<NewsEntity> getEntity() {
        return entity;
    }

    public Error getError() {
        return error;
    }

    public NewsEntityModel(NewsEntityState state, List<NewsEntity> entity, Error error) {
        this.state = state;
        this.entity = entity;
        this.error = error;
    }
}
