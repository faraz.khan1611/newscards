package com.newsapp.view.viewModels;

import android.os.Handler;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.newsapp.businessLayer.IInteractorInterface;
import com.newsapp.businessLayer.InteractorConsumer;
import com.newsapp.businessLayer.entity.NewsEntity;
import com.newsapp.businessLayer.network.Error;

import java.util.List;

/**
 * Created by Faraz Khan.
 */

public class NewsDataViewModel extends ViewModel implements InteractorConsumer {

    private final MutableLiveData<NewsEntityModel> newsListLiveData = new MutableLiveData<>();
    private final Handler handler = new Handler();

    public MutableLiveData<NewsEntityModel> getNewsListLiveData() {
        return newsListLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        handler.removeCallbacksAndMessages(null);
    }

    public void getNewsList(IInteractorInterface interactor) {
        interactor.loadNewsList(this);
    }

    //region InteractorConsumer implementation
    @Override
    public void accept(List<NewsEntity> data) {
        newsListLiveData.postValue(new NewsEntityModel(NewsEntityState.SUCCESS, data, null));
    }

    @Override
    public void reject(Error error) {
        newsListLiveData.postValue(new NewsEntityModel(NewsEntityState.FAIL, null, error));
    }
    //endregion
}
