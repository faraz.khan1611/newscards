This is a news app that is supposed to display news list in card view.

## About the project
The app follow clean design principal.
All the data is coming from the web endpoint.
Project uses androidx framework. The Design consist of android cardView.
The response contains a list of ids for news items.
The app has sorting implemented, which sorts based on Popularity and Recent

## Unit test
The app has 100% of testable business logic unit test coverage and 75% of total business logic converage


## Notes
1. To test the coverage for the app , Right click on Test package inside project -> click on run "All Test" with coverage
2. The app has efficient data caching implemented to avoid network call if possible.
3. The app uses LiveData for lifecycle aware callbacks.
4. The app has its own implementaion of dependency injection implementation to avoid singleton creations


## ScreenShots


![Webp.net-resizeimage__3_](/uploads/bc9df6f87378193c880af6c7c48a5b47/Webp.net-resizeimage__3_.png)


![Webp.net-resizeimage__4_](/uploads/3bf2695531db372d94ac3897d67044b0/Webp.net-resizeimage__4_.png)